package com.snipey92.timt.functions;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.snipey92.timt.Main;
import com.snipey92.timt.util.Messages;

public class Detective {
	Detective detective;
	Player player = (Player) this.detective;
	Inventory inv = this.player.getInventory();
	ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
	ItemStack armor = new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1);
	ItemStack book = new ItemStack(Material.BOOK, 1);
	PlayerInteractEvent event;
	private Main plugin;

	public Detective(Player player) {
		this.player = player;
		onDetectiveSpawn();
	}

	public void onDetectiveSpawn() {
		this.inv.addItem(new ItemStack[] { this.sword });
		this.inv.addItem(new ItemStack[] { this.armor });
		Bukkit.getServer().broadcastMessage(
				Messages.prefix + this.player.getDisplayName()
						+ "Is the detective!");
	}

	public void hitDetective(EntityDamageByEntityEvent event) {
		Player guy = (Player) event.getDamager();
		Player target = (Player) event;

		if (target.isDead()) {
			if ((guy != null)
					&& (TraitorFunctions.traitors
							.contains(guy.getDisplayName()))) {
				Bukkit.getServer().broadcastMessage(
						"The Detective was killed by a Traitor!");
			} else if ((guy != null) && (InnocentFunctions.isInnocent))
				Bukkit.getServer().broadcastMessage(
						"The Detective was killed by an Innocent!");
		}
	}

	public String isTraitor(Player guy) {
		if (TraitorFunctions.traitors.contains(guy.getDisplayName())) {
			return "Traitor";
		}
		return "Innocent";
	}

	public void dropBook(Player dead, boolean isDead) {
		while (dead.isDead()) {
			this.inv.addItem(new ItemStack[] { this.book });
			Action action = this.event.getAction();
			Player killer = dead.getKiller();
			if ((this.player.getItemInHand() != this.book)
					|| (action != Action.RIGHT_CLICK_BLOCK))
				continue;
			this.player.sendMessage("----------------------------------------");
			this.player.sendMessage("Player Name: " + dead.getDisplayName());
			this.player.sendMessage("Status: " + isTraitor(dead));
			this.player.sendMessage("Killer: " + killer.getDisplayName());
			this.player.sendMessage("----------------------------------------");
		}
	}
}