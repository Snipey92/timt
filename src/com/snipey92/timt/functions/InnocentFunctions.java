package com.snipey92.timt.functions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.Inventory;
import com.snipey92.timt.Main;
import com.snipey92.timt.util.Messages;

public class InnocentFunctions implements Innocent {
	Player innocent = null;
	Player traitor = null;
	Player other = null;
	Inventory inv;
	static boolean isTraitor;
	static boolean isInnocent;
	public static int traitorPoints;
	public static int traitorKarma;
	public static int innocentKarma;
	public static int innocentPoints;
	private Main plugin;

	public InnocentFunctions(Player player) {
		onInnocentSpawn(player);
	}

	public void onInnocentSpawn(Player player) {
		if (!TraitorFunctions.traitors.contains(this.innocent))
			isTraitor = true;
		else {
			isInnocent = true;
		}

	}

	public void hitInnocent(EntityDamageByEntityEvent event) {
		Player guy = (Player) event.getDamager();
		Player target = (Player) event;

		if (target.isDead()) {
			Location l = target.getLocation();
			l.getWorld().strikeLightningEffect(l);
			if ((guy != null) && (isInnocent) && (target != null)
					&& (isInnocent)) {
				Bukkit.getServer().broadcastMessage(
						Messages.prefix
								+ "An Innocent was killed by an Innocent!");
				innocentPoints -= 1;
				innocentKarma -= 1;
			}
		}
	}
}