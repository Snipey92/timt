package com.snipey92.timt.functions;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public abstract interface Innocent {
	public abstract void onInnocentSpawn(Player paramPlayer);

	public abstract void hitInnocent(
			EntityDamageByEntityEvent paramEntityDamageByEntityEvent);
}