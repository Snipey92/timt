package com.snipey92.timt.functions;

import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import com.snipey92.timt.Main;
import com.snipey92.timt.util.Messages;

public class TraitorFunctions implements Traitor {
	private Main plugin;
	static Player innocent = null;
	static Player traitor = null;
	Player other = null;
	Inventory traitorInventory;
	static boolean isTraitor;
	static boolean isInnocent;
	public static int traitorPoints;
	public static int traitorKarma;
	public static int innocentKarma;
	public static int innocentPoints;
	public static ArrayList<Object> traitors = new ArrayList();

	Random random = new Random(14L);
	ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE, 1);

	public TraitorFunctions(Player trait) {
		onTraitorSpawn(trait);
	}

	public void onTraitorSpawn(Player player) {
		if (traitors.contains(player.getDisplayName()))
			;
		isTraitor = true;
	}

	public void hitTraitor(EntityDamageByEntityEvent event) {
		Player attacker = (Player) event.getDamager();
		Player target = (Player) event;

		if ((attacker != null) && (isTraitor) && (target != null)
				&& (isTraitor)
				&& (traitors.contains(attacker.getDisplayName()))
				&& (traitors.contains(target.getDisplayName()))) {
			attacker.sendMessage(Messages.prefix + target.getDisplayName()
					+ "Is a fellow traitor. You might not want to hit him!");
		} else if (target.isDead()) {
			Location l = target.getLocation();
			l.getWorld().strikeLightningEffect(l);
			Bukkit.getServer().broadcastMessage(
					Messages.prefix + "A Traitor was killed by a Traitor!");
			traitorPoints -= 1;
			traitorKarma -= 1;
		} else if ((attacker != null) && (isTraitor) && (target != null)
				&& (isInnocent)
				&& (traitors.contains(attacker.getDisplayName()))) {
			Location l = target.getLocation();
			l.getWorld().strikeLightningEffect(l);
			Bukkit.getServer()
					.broadcastMessage(
							Messages.prefix
									+ "An Innocent was killed by a Traitor!");
			traitorPoints += 1;
		}
	}
}