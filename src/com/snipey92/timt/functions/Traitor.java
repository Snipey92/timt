package com.snipey92.timt.functions;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public abstract interface Traitor {
	public abstract void onTraitorSpawn(Player paramPlayer);

	public abstract void hitTraitor(
			EntityDamageByEntityEvent paramEntityDamageByEntityEvent);
}