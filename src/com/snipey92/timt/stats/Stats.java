package com.snipey92.timt.stats;

import java.util.ArrayList;
import org.bukkit.entity.Player;

import com.snipey92.timt.functions.InnocentFunctions;
import com.snipey92.timt.functions.TraitorFunctions;

public class Stats
{
  static Player player = null;
  static int points;
  static int karma;

  public static void giveStats()
  {
    player.sendMessage("-------------------");
    player.sendMessage("Your're a: " + isTraitor());
    player.sendMessage("Points: " + points);
    player.sendMessage("Karma: " + karma);
    player.sendMessage("-------------------");
  }

  public static String isTraitor()
  {
    if (TraitorFunctions.traitors.contains(player.getDisplayName())) {
      points = TraitorFunctions.traitorPoints;
      karma = TraitorFunctions.traitorKarma;
      return "Traitor!";
    }
    points = InnocentFunctions.innocentPoints;
    karma = InnocentFunctions.innocentKarma;
    return "Innocent!";
  }
}