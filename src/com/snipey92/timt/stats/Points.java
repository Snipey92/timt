package com.snipey92.timt.stats;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import com.snipey92.timt.Main;
import com.snipey92.timt.enums.GameState;
import com.snipey92.timt.functions.InnocentFunctions;
import com.snipey92.timt.functions.TraitorFunctions;
import com.snipey92.timt.util.Messages;

public class Points
{
  int points;
  int karma;
  Player innocent = null;
  Player traitor = null;
  int innocentPoints;
  int innocentKarma;
  static boolean innocentWin = false;
  static boolean traitorWin = false;
  static boolean tie;
  static int i;
  private Main plugin;

  public void getPoints()
  {
    TraitorFunctions.traitorPoints = this.points;
    TraitorFunctions.traitorKarma = this.karma;
    int traitorPoints = this.points - this.karma;
    InnocentFunctions.innocentPoints = this.innocentPoints;
    InnocentFunctions.innocentKarma = this.innocentKarma;
    int innocentFinal = this.innocentPoints - this.innocentKarma;
    if (innocentFinal < traitorPoints) {
      traitorWin = true;
    }

    if (traitorPoints > innocentFinal) {
      innocentWin = true;
    }

    if (innocentFinal == traitorPoints)
      tie = true;
  }

  public void innocentWin()
  {
    if (this.plugin.gamestate == GameState.INGAME) {
      Bukkit.getServer().broadcastMessage(Messages.prefix + this.innocent.getDisplayName() + Messages.innocentwin);
      Bukkit.getServer().broadcastMessage("---------------------------------");
      Bukkit.getServer().broadcastMessage("Winner: " + this.innocent.getDisplayName());
      Bukkit.getServer().broadcastMessage("Points: " + this.innocentPoints);
      Bukkit.getServer().broadcastMessage("Karma: " + this.innocentKarma);
      Bukkit.getServer().broadcastMessage("The Traitors Were: " + TraitorFunctions.traitors);
      Bukkit.getServer().broadcastMessage("Did you have fun? Go check out our other servers, or visit us on the web @ lnmc.net");
      for (i = 0; i > Bukkit.getServer().getOnlinePlayers().length; )
      {
        Player[] igpls = Bukkit.getServer().getOnlinePlayers();
        igpls[i].kickPlayer("Server is reloading!");
      }
      Bukkit.getServer().reload();
    }
  }

  public void traitorWin() {
    if (this.plugin.gamestate == GameState.INGAME) {
      Bukkit.getServer().broadcastMessage(Messages.prefix + this.traitor.getDisplayName() + Messages.traitorwin);
      Bukkit.getServer().broadcastMessage("---------------------------------");
      Bukkit.getServer().broadcastMessage("Did you have fun? Go check out our other servers!");
      Bukkit.getServer().broadcastMessage("Winner: " + this.traitor.getDisplayName());
      Bukkit.getServer().broadcastMessage("Points: " + this.points);
      Bukkit.getServer().broadcastMessage("Karma: " + this.karma);
      Bukkit.getServer().broadcastMessage("The Other Traitors Were: " + TraitorFunctions.traitors);

      Player[] igpls = Bukkit.getServer().getOnlinePlayers();
      igpls[i].kickPlayer("Server is reloading!");

      Bukkit.getServer().reload();
    }
  }
	public void checkTeams(){
		if(this.plugin.detectives.size() == 0 && this.plugin.innocent.size() == 0){
			traitorWin();
		}else if(TraitorFunctions.traitors.size() == 0){
			innocentWin();
		}
	}
}