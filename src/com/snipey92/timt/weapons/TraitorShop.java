package com.snipey92.timt.weapons;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.snipey92.timt.Main;
import com.snipey92.timt.functions.TraitorFunctions;
import com.snipey92.timt.lib.IconMenu;
import com.snipey92.timt.weapons.specials.PoisonArrow;

public class TraitorShop extends Items {
	static ProjectileLaunchEvent event;
	static ProjectileHitEvent hit;
	static PoisonArrow poison = new PoisonArrow();
	static Main plugin;
	ItemStack arrow = new ItemStack(Material.ARROW, 20);
	ItemStack powersword = new ItemStack(Material.STONE_SWORD, 1);
	ItemStack bow = new ItemStack(Material.BOW, 1);


	public IconMenu menu = new IconMenu(ChatColor.RED + "Traitor Shop", 9,
			new IconMenu.OptionClickEventHandler() {
				@Override
				public void onOptionClick(IconMenu.OptionClickEvent event) {
					Player p = event.getPlayer();
					Inventory inv = event.getPlayer().getInventory();
					if (event.getName()
							.equals(ChatColor.GREEN + "Poison Arrow")) {
						if (TraitorFunctions.traitorPoints == 5) {
							TraitorFunctions.traitorPoints = 0;
							p.sendMessage("You bought a Poison Arrow!");
							inv.addItem(new ItemStack[] { arrow });
							if (event != null) {
								PoisonArrow.onPlayerHit(hit, p);
							}
						}
						event.setWillDestroy(true);
					}
					if (event.getName().equals("Power Sword")){
				        if (TraitorFunctions.traitorPoints == 2) {
				            TraitorFunctions.traitorPoints = 0;
				            p.sendMessage("You bought a Power Sword!");
				            inv.addItem(new ItemStack[] { powersword });
					        ItemMeta im = powersword.getItemMeta();
					        im.setDisplayName(ChatColor.DARK_PURPLE + "Power Sword");
					        powersword.setItemMeta(im);
				          }
				        }
				        else if (TraitorFunctions.traitorPoints < 2) {
				          p.sendMessage("You don't have enough points to buy this!");
				        } else {
				          if (TraitorFunctions.traitorPoints > 2) {
				            TraitorFunctions.traitorPoints -= 2;
				            p.sendMessage("You bought a Power Sword!");
				            inv.addItem(new ItemStack[] { powersword });
					        ItemMeta im = powersword.getItemMeta();
					        im.setDisplayName(ChatColor.DARK_PURPLE + "Power Sword");
					        powersword.setItemMeta(im);
				          }
						event.setWillDestroy(true);
					}
					event.setWillClose(true);
				}
			}, plugin)
			.setOption(0, new ItemStack(Material.PAPER, 1),
					ChatColor.GOLD + "Points",
					Integer.toString(TraitorFunctions.traitorPoints))
			.setOption(3, new ItemStack(Material.BOW, 1),
					ChatColor.GREEN + "Poison Bow",
					"Poison your enemies for only 5 tokens.")
			.setOption(4, new ItemStack(Material.GOLD_SWORD, 1), "Power Sword",
					"Weapons are for awesome people")
			.setOption(5, new ItemStack(Material.TNT, 1), "Jihad",
					"Explosives brings happiness");
}
/*public static boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args) {

Player buy = null;
if (Stats.isTraitor() == "Traitor!")
{
  Inventory inv = buy.getInventory();
  ItemStack arrow = new ItemStack(Material.ARROW, 20);
  ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
  ItemStack bow = new ItemStack(Material.BOW, 1);

  if (command.getName().equalsIgnoreCase("tshop")) {
	  
	  
    buy.sendMessage(ChatColor.GREEN + "---------------------------");
    buy.sendMessage(ChatColor.GREEN + "Your Points: " + TraitorFunctions.traitorPoints);
    buy.sendMessage(ChatColor.GREEN + "Use /traitorshop (item) to buy an item.");
    buy.sendMessage(ChatColor.GREEN + "Poison Arrows - 5 points");
    buy.sendMessage(ChatColor.GREEN + "Sword (type sword) - 2 points");
    buy.sendMessage(ChatColor.GREEN + "Bow (type bow) - 3 points");
    buy.sendMessage(ChatColor.GREEN + "---------------------------");
    if (args[0] == "poisonarrows") {
      if (TraitorFunctions.traitorPoints == 5) {
        TraitorFunctions.traitorPoints = 0;
        buy.sendMessage("You bought a Poison Arrow!");
        inv.addItem(new ItemStack[] { arrow });
        if (event != null) {
          PoisonArrow.onPlayerHit(hit, buy);
        }
      }
      else if (TraitorFunctions.traitorPoints < 5) {
        buy.sendMessage("You don't have enough points to buy this!");
      }

    }
    else if (TraitorFunctions.traitorPoints > 5) {
      TraitorFunctions.traitorPoints -= 5;
      buy.sendMessage("You bought a Poison Arrow!");
      inv.addItem(new ItemStack[] { arrow });
      if (event != null) {
        PoisonArrow.onPlayerHit(hit, buy);
      }
      if (args[0] == "sword") {
        if (TraitorFunctions.traitorPoints == 2) {
          TraitorFunctions.traitorPoints = 0;
          buy.sendMessage("You bought a Stone Sword!");
          inv.addItem(new ItemStack[] { sword });
        }
      }
      else if (TraitorFunctions.traitorPoints < 2) {
        buy.sendMessage("You don't have enough points to buy this!");
      } else {
        if (TraitorFunctions.traitorPoints > 2) {
          TraitorFunctions.traitorPoints -= 2;
          buy.sendMessage("You bought a Stone Sword!");
          inv.addItem(new ItemStack[] { sword });
        }

        if (args[0] == "bow") {
          if (TraitorFunctions.traitorPoints == 3) {
            TraitorFunctions.traitorPoints = 0;
            buy.sendMessage("You bought a Bow!");
            inv.addItem(new ItemStack[] { bow });
          }
          else if (TraitorFunctions.traitorPoints < 3) {
            buy.sendMessage("You don't have enough points!");
          }
          else if (TraitorFunctions.traitorPoints > 3) {
            TraitorFunctions.traitorPoints -= 3;
            buy.sendMessage("You bought a Bow!");
            inv.addItem(new ItemStack[] { bow });
          }
        }

      }

    }

  }
  else
  {
    buy.sendMessage(ChatColor.RED + "You're not a Traitor!");
  }
}
return false;
}
}*/
