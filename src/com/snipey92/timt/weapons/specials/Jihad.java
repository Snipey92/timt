package com.snipey92.timt.weapons.specials;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class Jihad {
	public void onBlockPlace(BlockPlaceEvent event){
		if (event.getBlock().equals(Material.TNT)){
			event.setCancelled(true);
		}
	}

	public void onRightClick(PlayerInteractEvent event){
		Action a = event.getAction();
		if (a.RIGHT_CLICK_AIR != null){
			if (event.useItemInHand().equals("Jihad")){
				Location loc = event.getPlayer().getLocation();
				TNTPrimed tnt = loc.getWorld().spawn(loc, TNTPrimed.class);
				tnt.setFuseTicks(1);
				tnt.setYield(4);
			}
		}
	}
}
