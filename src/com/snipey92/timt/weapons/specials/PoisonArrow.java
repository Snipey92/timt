package com.snipey92.timt.weapons.specials;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PoisonArrow {
	  public static void onPlayerHit(ProjectileHitEvent event, Player player)
	  {
	    player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 1));
	  }
}
