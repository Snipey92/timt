/**
 * 
 */
/**
 * @author Snipey
 *
 */
package com.snipey92.timt.updater;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.bukkit.entity.Player;

import com.snipey92.timt.Main;
import com.snipey92.timt.util.Messages;

public class Updater {
	
	private Main plugin;
	private boolean update = false;
	
	public boolean update() {
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(
							new URL(
									"https://dl.dropboxusercontent.com/u/42853892/bukkitplugins/timt/version.txt")
									.openStream()));
			this.plugin.v = br.readLine();
			br.close();
			if (!this.plugin.getDescription().getVersion().equalsIgnoreCase(this.plugin.v)) {
				this.plugin.send("!!!!!~~~~~NEW VERSION OF TIMT~~~~~!!!!!");
				this.plugin.send("Current version: " + this.plugin.getDescription().getVersion()
						+ ", New version: " + this.plugin.v);
				this.plugin.send("Attempting to download..");

				BufferedInputStream in = new BufferedInputStream(
						new URL(
								"https://dl.dropboxusercontent.com/u/42853892/bukkitplugins/timt/TIMT.jar")
								.openStream());
				FileOutputStream fos = new FileOutputStream(new File(
						"plugins/TIMT.jar"));
				byte d[] = new byte[1024];
				int count;
				while ((count = in.read(d, 0, 1024)) != -1) {
					fos.write(d, 0, count);
				}
				in.close();
				fos.close();
				this.plugin.send("Successfully updated TIMT. Reload or restart to see changes");
				return true;
			} else {
				this.plugin.send("You have the latest version");
			}
			return false;
		} catch (IOException e) {
			this.plugin.sendErr("Failed to download update or check for update");
			e.printStackTrace();
			return false;
		}
	}

	public void updateCheck() {
		// TODO Auto-generated method stub
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(
							new URL(
									"https://dl.dropboxusercontent.com/u/42853892/bukkitplugins/timt/version.txt")
									.openStream()));
			this.plugin.v = br.readLine();
			br.close();
			if (!this.plugin.getDescription().getVersion().equalsIgnoreCase(this.plugin.v)) {
				Player p = null;
				if(p.hasPermission("timt.update")){
					p.sendMessage(Messages.prefix + "Update Available.");
				}
			}
		} catch (IOException e) {
			this.plugin.sendErr("Failed to download update or check for update");
			e.printStackTrace();
			return;
		}
	}
}