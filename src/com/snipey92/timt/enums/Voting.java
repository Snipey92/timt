package com.snipey92.timt.enums;

public enum Voting {
	INPROGRESS, FINISHED, CANCELLED, PREVOTE, POSTVOTE;
}
