package com.snipey92.timt.enums;

public enum CountdownState {
	COUNTING_DOWN, GAME_STARTED, COUNTDOWN_CANCELED, RESUME, VOTING, FINISHEDVOTING, LOADING;
}
