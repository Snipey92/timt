package com.snipey92.timt.enums;

public enum GameState {
	VIP, STAFF, EDIT, PREGAME, INGAME, RESET;
}
