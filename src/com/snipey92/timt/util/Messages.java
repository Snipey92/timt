/**
 * 
 */
/**
 * @author Snipey
 *
 */
package com.snipey92.timt.util;

import org.bukkit.ChatColor;

import com.snipey92.timt.Main;

public class Messages{
	
	private Main plugin;
	//private int size = this.plugin.playerlist.size();

	public static String prefix = (ChatColor.AQUA + "[" + ChatColor.BOLD + "" + ChatColor.GOLD + "TIMT" + ChatColor.AQUA + "] ");
    public static String kickMessage = prefix + "Kicked for ";
    public static String tempBanMessage = prefix + "You have been " + ChatColor.AQUA + "Temporary Banned" + ChatColor.DARK_AQUA + " for ";
    public static String permBanMessage = prefix + "You have been " + ChatColor.AQUA + "Permanently Banned" + ChatColor.DARK_AQUA + "!";
    
    public static String staffStateMessage = prefix + "This server is currently in " + ChatColor.AQUA + "STAFF-ONLY" + ChatColor.DARK_AQUA + " mode.";
    public static String editStateMessage = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    
    public static String secondLobby = prefix + ChatColor.AQUA + "%%TIME%%" + ChatColor.DARK_AQUA + " second(s) remains until lobby ends.";
    public static String minuteLobby = prefix + ChatColor.AQUA + "%%TIME%%" + ChatColor.DARK_AQUA + " minutes(s) remains until game ends.";
    public static String hourLobby = prefix + ChatColor.AQUA + "%%TIME%%" + ChatColor.DARK_AQUA + " hour(s) remains until game ends.";
    
    public static String updatesuccess = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    public static String updatefailed = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    
    public static String traitor = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    public static String detective = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    public static String innocent = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    
    public static String innocentwin = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    public static String traitorwin = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    
    public static String restarting = prefix + "This server is currently in " + ChatColor.AQUA + "EDIT" + ChatColor.DARK_AQUA + " mode.";
    
    //public static String remaining = prefix + ChatColor.GOLD + "(" + ChatColor.GREEN + size + ChatColor.GOLD + "/" + ChatColor.GREEN + "24" + ChatColor.GOLD + ")";
}