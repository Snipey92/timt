package com.snipey92.timt;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.snipey92.timt.commands.MainCommands;
import com.snipey92.timt.enums.CountdownState;
import com.snipey92.timt.enums.GameState;
import com.snipey92.timt.enums.GameTeam;
import com.snipey92.timt.enums.Voting;
import com.snipey92.timt.functions.TraitorFunctions;
import com.snipey92.timt.listeners.ButtonPressEvent;
import com.snipey92.timt.listeners.ChestListener;
import com.snipey92.timt.listeners.TIMTListener;
import com.snipey92.timt.runnable.AlwaysRunning;
import com.snipey92.timt.runnable.CountDown;
import com.snipey92.timt.util.Messages;
import com.snipey92.timt.weapons.TraitorShop;

public class Main extends JavaPlugin {

	Random generator = new Random();
	File config = new File("plugins/TIMT/config.yml");

	public ArrayList<String> editmode = new ArrayList<String>();
	public HashMap<String, GameTeam> playersIngame = new HashMap<String, GameTeam>();
	public ArrayList<String> playerlist = new ArrayList<String>();
	public HashMap<Integer, String> deadInfos = new HashMap<Integer, String>();
	public ArrayList<String> detectives = new ArrayList<String>();
	public ArrayList<String> innocent = new ArrayList<String>();
	public ArrayList<String> selectedmaps = new ArrayList<String>();
	public ArrayList<String> voteMap3 = new ArrayList<String>();
	public ArrayList<String> voteMap2 = new ArrayList<String>();
	public ArrayList<String> voteMap1 = new ArrayList<String>();
	public ArrayList<String> playersFinalBox = new ArrayList<String>();

	public int votes;
	public boolean peace;
	private boolean debug;
	public boolean loaded;
	public boolean finished = false;


	public String map1 = "";
	public String map2 = "";
	public String map3 = "";
	public String v;
	public String selectedMap = "";

	public int x;
	public int y;
	public int z;

	public CountdownState state;
	public GameState gamestate;
	public Voting voting;
	public TraitorShop tshop;


	private static Main instance;

	public void onEnable() {
		this.gamestate = GameState.PREGAME;
		this.voting = Voting.PREVOTE;
		instance = this;
		registerCommands();
		registerEvents();
		loadConfig();
		alwaysRunning();

	}

	public void registerCommands() {
		getCommand("timt").setExecutor(new MainCommands(this));
	}

	public void registerEvents() {
		Bukkit.getPluginManager().registerEvents(new TIMTListener(this), this);
		Bukkit.getPluginManager().registerEvents(new ButtonPressEvent(this),
				this);
		Bukkit.getPluginManager().registerEvents(new ChestListener(this),
				this);
	}

	private void loadConfig() {
		if (!this.config.exists()) {
			getConfig().options().header("TIMT, Made By Snipey92");
			getConfig()
					.addDefault("TIMT.system.prefix.player", Messages.prefix);
			getConfig().addDefault("TIMT.system.debug", false);
			getConfig().options().copyDefaults(true);
			saveConfig();
		}
	}



	public void send(String message) {
		this.getLogger().log(Level.INFO, message);
	}

	public static void sendErr(String error) {
		System.out.println(Messages.prefix + " [Error] " + error);
	}

	public void debug(String message) {
		if (debug)
			System.out.println(Messages.prefix + "[Debug] " + message);
	}

	public boolean shouldDebug() {
		return getConfig().getBoolean("debug", false);
	}

	public void startGame() {
		this.gamestate = GameState.INGAME;
		teleportStart();
		this.peace = true;
		sendArenaMessage(ChatColor.GREEN + Messages.prefix
				+ " 30 second peace period.");
		sendArenaMessage("�e�lPlugin Made By �k|�fSnipey92�e�k|");
		getServer().getScheduler().scheduleSyncDelayedTask(this,
				new Runnable() {
					public void run() {
						Main.this.setPlayers();
						Main.this.peace = false;

						Main.this.sendArenaMessage(ChatColor.RED
								+ Messages.prefix + " Peace period has ended.");
					}
				}, 600L);

	}

	public void setPlayers() {
		for (int x = 0; x <= playerlist.size() - 1; x++) {
			String player1 = (String) playerlist.get(x);
			if (x % 5 == 1) {
				TraitorFunctions.traitors.add(player1);

			} else if ((x % 5 == 2) && (x < 15)) {
				detectives.add(player1);
			} else if ((x % 5 == 0) || (x % 3 == 0) || (x % 4 == 0)) {
				this.innocent.add(player1);
			}
		}
	}

	private void teleportStart() {


	if(Bukkit.getServer().getWorld(selectedMap) != null){
		for (String playername : playerlist) {
			Player player = getServer().getPlayerExact(playername);
			if (player != null) {
				player.teleport(Bukkit.getWorld(selectedMap).getSpawnLocation());
			}
		}
		}
	}

	public void sendArenaMessage(String message) {
		Bukkit.broadcastMessage(message);
	}

	public void getRandomMap() {
		File f = new File("plugins/TIMT/Maps");
		ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));
		Collections.shuffle(names);
		String map1 = names.get(0);
		String map2 = names.get(1);
		String map3 = names.get(2);
		selectedmaps.add(map1);
		selectedmaps.add(map2);
		selectedmaps.add(map3);
	}

	public void countdown() {
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this,
				new CountDown(this), 0L, 20L);
		}
	

	public void voting() {
		//getServer().getScheduler().scheduleSyncRepeatingTask(this, new Voting(this), 0L, 20L);
		
		
		getRandomMap();
		sendArenaMessage(ChatColor.GREEN + Messages.prefix
				+ "Voting Begins" + ChatColor.GOLD + " NOW"
				+ ChatColor.GREEN + "!");
		map1 = selectedmaps.get(0);
		map2 = selectedmaps.get(1);
		map3 = selectedmaps.get(2);
		sendArenaMessage(ChatColor.GOLD
				+ "----------------------------------------------");
		sendArenaMessage(ChatColor.AQUA
				+ "Your choices are:");
		sendArenaMessage(ChatColor.AQUA + "1. "
				+ map1);
		sendArenaMessage(ChatColor.AQUA + "2. "
				+ map2);
		sendArenaMessage(ChatColor.AQUA + "3. "
				+ map3);
		sendArenaMessage(ChatColor.AQUA
				+ "Use '/vote [number]' to cast your vote");
		sendArenaMessage(ChatColor.GOLD
				+ "----------------------------------------------");
		voting = Voting.INPROGRESS;
	}

	public void alwaysRunning() {
		getServer().getScheduler().scheduleSyncRepeatingTask(this,
				new AlwaysRunning(this), 0L, 20L);
	}

	public static Main getInstance() {
		return instance;
	}
	
	
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
	if (command.getName().equalsIgnoreCase("tshop")) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("timt.player")) {
				if(TraitorFunctions.traitors.contains(player.getName())){
					tshop.menu.open(player);
				}else{
					player.sendMessage(Messages.prefix + "You are not a traitor");
				}
			} else {
				player.sendMessage(ChatColor.RED
						+ "You do not have permission (timt.player)");
				return true;
			}
		} else {
			sender.sendMessage(ChatColor.RED
					+ "You must be a player to use that command");
			return true;
		}
	}
	if (command.getName().equalsIgnoreCase("vote")) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (player.hasPermission("timt.player")){
				if(voting == Voting.INPROGRESS){
				if (args[0].contains("1")){
					if (voteMap2.contains(player.getName())){
						voteMap2.remove(player.getName());
						voteMap1.add(player.getName());
						player.sendMessage(Messages.prefix + "You changed your vote to: " + ChatColor.GOLD + map1);
					}else if(voteMap3.contains(player.getName())){
						voteMap3.remove(player.getName());
						voteMap1.add(player.getName());
						player.sendMessage(Messages.prefix + "You changed your vote to: " + ChatColor.GOLD + map1);
					}else if(!(voteMap2.contains(player.getName()) && !(voteMap3.contains(player.getName())))){
						voteMap1.add(player.getName());
						player.sendMessage(Messages.prefix + "You voted for: " + ChatColor.GOLD + map1);
					}

				}else if(args[0].contains("2")){
					if (voteMap1.contains(player.getName())){
						voteMap1.remove(player.getName());
						voteMap2.add(player.getName());
						player.sendMessage(Messages.prefix + "You changed your vote to: " + ChatColor.GOLD + map2);
					}else if(voteMap3.contains(player.getName())){
						voteMap3.remove(player.getName());
						voteMap2.add(player.getName());
						player.sendMessage(Messages.prefix + "You changed your vote to: " + ChatColor.GOLD + map2);
					}else if(!(voteMap1.contains(player.getName()) && !(voteMap3.contains(player.getName())))){
						voteMap2.add(player.getName());
						player.sendMessage(Messages.prefix + "You voted for: " + ChatColor.GOLD + map2);
					}
				}else if(args[0].contains("3")){
					if (voteMap2.contains(player.getName())){
						voteMap2.remove(player.getName());
						voteMap3.add(player.getName());
						player.sendMessage(Messages.prefix + "You changed your vote to: " + ChatColor.GOLD + map3);
					}else if(voteMap1.contains(player.getName())){
						voteMap1.remove(player.getName());
						voteMap3.add(player.getName());
						player.sendMessage(Messages.prefix + "You changed your vote to: " + ChatColor.GOLD + map3);
					}else if(!(voteMap2.contains(player.getName()) && !(voteMap1.contains(player.getName())))){
						voteMap3.add(player.getName());
						player.sendMessage(Messages.prefix + "You voted for: " + ChatColor.GOLD + map3);
					}
				}
			}
			}
			else {
				player.sendMessage(ChatColor.RED
					+ "You do not have permission (timt.player)");
				return true;
		}
		}
	}
	return false;
	}
}