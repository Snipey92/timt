package com.snipey92.timt.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.snipey92.timt.Main;
import com.snipey92.timt.functions.TraitorFunctions;
import com.snipey92.timt.stats.Stats;
import com.snipey92.timt.util.Messages;
import com.snipey92.timt.weapons.TraitorShop;
import com.snipey92.timt.weapons.specials.PoisonArrow;

public class MainCommands implements CommandExecutor {

	static ProjectileLaunchEvent event;
	static ProjectileHitEvent hit;
	static PoisonArrow poison = new PoisonArrow();
	private Main plugin;
	private TraitorShop tshop;
	public MainCommands(Main main) {
		this.plugin = main;
		
	}

	private void help(CommandSender s) {
		
		s.sendMessage(ChatColor.GREEN + "----------------------------------------------");
		s.sendMessage(ChatColor.RED + "TIMT Commands You Can Use");
		if (s.hasPermission("timt.create")) s.sendMessage(ChatColor.GOLD + "/timt edit");
		if (s.hasPermission("timt.create")) s.sendMessage(ChatColor.GOLD + "/timt save");
		if (s.hasPermission("timt.create")) s.sendMessage(ChatColor.GOLD + "/timt setlobby");
		if (s.hasPermission("timt.info")) s.sendMessage(ChatColor.GOLD + "/timt info");
		if (s.hasPermission("timt.create")) s.sendMessage(ChatColor.GOLD + "/timt setgame");
		//if (s.hasPermission("timt.player")) s.sendMessage(ChatColor.GOLD + "/timt leave");
		s.sendMessage(ChatColor.GREEN + "----------------------------------------------");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String world = "";
		if (args.length == 0) {
			help(sender);
			return true;
		}
		if (args[0].equalsIgnoreCase("edit")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.create")) {
					if (!this.plugin.editmode.contains(player.getPlayer().getName())){
						this.plugin.editmode.add(player.getPlayer().getName());
						sender.sendMessage(Messages.prefix + "Edit mode enabled.");
					}else if (this.plugin.editmode.contains(player.getPlayer().getName())){
						sender.sendMessage(Messages.prefix + "Edit mode is already enabled.");
					}
						

					} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.create)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}
		else if (args[0].equalsIgnoreCase("save")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.create")) {
					if (this.plugin.editmode.contains(player.getPlayer().getName())){
						this.plugin.editmode.remove(player.getPlayer().getName());
						sender.sendMessage(Messages.prefix + "Edit mode disabled.");
					}else if(!this.plugin.editmode.contains(player.getPlayer().getName())){
						sender.sendMessage(Messages.prefix + "You are not in edit mode.");
					}
				} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.create)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}
		else if (args[0].equalsIgnoreCase("setlobby")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.create")) {
					if (this.plugin.editmode.contains(player.getPlayer().getName())){						
				          int posX = player.getLocation().getBlockX();
				          int posY = player.getLocation().getBlockY();
				          int posZ = player.getLocation().getBlockZ();
				          double yaw = player.getLocation().getPitch();
				          double pitch = player.getLocation().getYaw();
				          world = player.getLocation().getWorld().getName();
				          this.plugin.getConfig().set("TIMT.spawn.lobby.posX", Integer.valueOf(posX));
				          this.plugin.getConfig().set("TIMT.spawn.lobby.posY", Integer.valueOf(posY));
				          this.plugin.getConfig().set("TIMT.spawn.lobby.posZ", Integer.valueOf(posZ));
				          this.plugin.getConfig().set("TIMT.spawn.lobby.yaw", Double.valueOf(yaw));
				          this.plugin.getConfig().set("TIMT.spawn.lobby.pitch", Double.valueOf(pitch));
				          this.plugin.getConfig().set("TIMT.spawn.lobby.world", world);
				          this.plugin.saveConfig();
						sender.sendMessage(Messages.prefix + "You have set the lobby spawn.");
					}else if(!this.plugin.editmode.contains(player.getPlayer().getName())){
						sender.sendMessage(Messages.prefix + "You are not in edit mode.");
					}
				} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.create)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}
		else if (args[0].equalsIgnoreCase("setgame")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.create")) {
					if (this.plugin.editmode.contains(player.getPlayer().getName())){						
				          int posX = player.getLocation().getBlockX();
				          int posY = player.getLocation().getBlockY();
				          int posZ = player.getLocation().getBlockZ();
				          double yaw = player.getLocation().getPitch();
				          double pitch = player.getLocation().getYaw();
				          world = player.getLocation().getWorld().getName();
				          this.plugin.getConfig().set("TIMT.spawn.game.posX", Integer.valueOf(posX));
				          this.plugin.getConfig().set("TIMT.spawn.game.posY", Integer.valueOf(posY));
				          this.plugin.getConfig().set("TIMT.spawn.game.posZ", Integer.valueOf(posZ));
				          this.plugin.getConfig().set("TIMT.spawn.game.yaw", Double.valueOf(yaw));
				          this.plugin.getConfig().set("TIMT.spawn.game.pitch", Double.valueOf(pitch));
				          this.plugin.getConfig().set("TIMT.spawn.game.world", world);
				          this.plugin.saveConfig();
						sender.sendMessage(Messages.prefix + "You have set the game spawn.");
					}else if(!this.plugin.editmode.contains(player.getPlayer().getName())){
						sender.sendMessage(Messages.prefix + "You are not in edit mode.");
					}
				} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.create)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}
		else if (args[0].equalsIgnoreCase("leave")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.player")) {
					this.plugin.playerlist.remove(player.getName());
					this.plugin.playersIngame.remove(player.getName());
					sender.sendMessage(Messages.prefix + "You left the game.");
					player.kickPlayer(Messages.prefix + "You left the game");
				} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.player)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}
		else if (args[0].equalsIgnoreCase("info")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.dev")) {
	 	              player.sendMessage(ChatColor.RED + "----------------------------------------------");
		              player.sendMessage(ChatColor.GREEN + Messages.prefix + " Players: " + this.plugin.playerlist.size());
		              player.sendMessage(ChatColor.GREEN + Messages.prefix + " Players in EditMode: " + this.plugin.editmode.size());
		              player.sendMessage(ChatColor.GREEN + Messages.prefix + " Version: " + this.plugin.getDescription().getVersion());
		              player.sendMessage(ChatColor.GREEN + Messages.prefix + " Author: Snipey92");
		              player.sendMessage(ChatColor.RED + "----------------------------------------------");
					
				} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.info)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}
		/*else if (args[0].equalsIgnoreCase("skip")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.skip")) {
			 		this.plugin.skip += 1;
			         if (this.plugin.skip > Bukkit.getOnlinePlayers().length / 2) {
			           this.plugin.skipped = true;
			           Bukkit.broadcastMessage(Messages.prefix + ChatColor.GOLD + "Map: " + this.plugin.map1 + " was skipped!");
			           Bukkit.broadcastMessage(Messages.prefix + ChatColor.GOLD + "New Map: " + this.plugin.map2);
			         }
					
				} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.info)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}*/
		else if (args[0].equalsIgnoreCase("test")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				if (player.hasPermission("timt.dev")) {

					
				} else {
					player.sendMessage(ChatColor.RED + "You do not have permission (timt.info)");
					return true;
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You must be a player to use that command");
				return true;
			}
		}
		
		//if (Stats.isTraitor().equals("Traitor!")) {


		
		return false;
	}

}
