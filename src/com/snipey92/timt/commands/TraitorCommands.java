package com.snipey92.timt.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.snipey92.timt.Main;
import com.snipey92.timt.functions.TraitorFunctions;
import com.snipey92.timt.stats.Stats;
import com.snipey92.timt.weapons.TraitorShop;
import com.snipey92.timt.weapons.specials.PoisonArrow;

public class TraitorCommands implements CommandExecutor {
	private TraitorShop tshop;

	private Main plugin;

	public TraitorCommands(Main main) {
		this.plugin = main;
		// TODO Auto-generated constructor stub
	}

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {

		if (Stats.isTraitor().equals("Traitor!")) {

			if (command.getName().equalsIgnoreCase("tshop")) {
				if (sender instanceof Player) {
					Player player = (Player) sender;
					if (player.hasPermission("timt.player")) {

						tshop.menu.open(player);

					} else {
						player.sendMessage(ChatColor.RED
								+ "You do not have permission (timt.create)");
						return true;
					}
				} else {
					sender.sendMessage(ChatColor.RED
							+ "You must be a player to use that command");
					return true;
				}
			}
		}
		return false;
	}
}
