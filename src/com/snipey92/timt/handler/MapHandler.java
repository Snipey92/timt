package com.snipey92.timt.handler;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.snipey92.timt.Main;
import com.snipey92.timt.enums.CountdownState;


public class MapHandler {
	
	private static File mapWorld;
	private static File serverfolder;
	public Main plugin;
	//public static boolean loaded = false;
	
	
	public static boolean checkMapExists(String MapName){
		mapWorld = new File(MapName);
			if (!mapWorld.exists())
				return false;
		return true;

	}
	 public static boolean load(String MapName) {
		  return load(MapName, false);
		 }
	 public static boolean load(String MapName, boolean save) {
		  Bukkit.getLogger().info("Loading world " + MapName);
		  World w = Bukkit.createWorld(new WorldCreator(MapName));
		  w.setAutoSave(false);
		  boolean loaded = (w != null);
		  Bukkit.getLogger().info("Loaded fully (" + MapName + "): " + loaded);
		  return loaded;
		 }
	 
	public static void copyLobby(String MapName){
			mapWorld = new File("plugins/TIMT/Maps/" + MapName);
			if (!mapWorld.exists()) {
				System.out.println("[ERROR] Lobby world could not be found.");
				mapWorld.mkdirs();
			}
			else {
				if (new File(mapWorld, "level.dat").exists()) {
					new File(mapWorld, "uid.dat").delete();
					if (!isCopied(MapName))
						try {
							Copier.copyFolder(mapWorld, new File(MapName));
						} catch (IOException e) {
							System.out.println("[ERROR] Lobby world couldn't be copied!");
						}
					if (Bukkit.getWorld(MapName) != null)
						System.out.println(MapName + " is still loaded!");
					MapHandler.load(MapName);
					for (LivingEntity e : Bukkit.getWorld(MapName).getEntitiesByClass(LivingEntity.class))
						if (!(e instanceof Player))
							e.setHealth(0);
				}
				else {
					System.out.println("[ERROR] Lobby world could not be found.");
				}
			}
		}

	public static boolean isCopied(String name) {
		File f = new File(name);
		return f.exists() && f.isDirectory();
	}
	/*public static void load(String MapName){
		mapWorld = new File("plugins/TIMT/Maps/" + MapName);
		serverfolder = new File(MapName);
		if(!mapWorld.exists()){
			Bukkit.getLogger().warning("The world " + MapName + " was not found.");
		}else{
			if (new File(mapWorld, "level.dat").exists()) {
				new File(mapWorld, "uid.dat").delete();
				if (Bukkit.getWorld(MapName) != null){
					Bukkit.getLogger().severe(MapName + " world is still loaded!");
					World world = Bukkit.getWorld(MapName);
					Bukkit.unloadWorld(world, false);
					Copier.deleteDirectory(serverfolder);
				}else if(Bukkit.getWorld(MapName) == null){
					try {
						Copier.copyFolder(mapWorld, new File(MapName));
					} catch (IOException e) {
						System.out.println("[ERROR] Map couldn't be copied!");
					}
				}

			}else{
				World w = Bukkit.createWorld(new WorldCreator(MapName));
				w.setAutoSave(false);
				w.setMonsterSpawnLimit(0);
	            System.out.println("Map Loaded");
			}
		}
	}*/
	/*public static void load(String MapName){
		
		mapWorld = new File("plugins/TIMT/Maps/" + MapName);
		if (!mapWorld.exists()) {
			Bukkit.getLogger().warning("The world " + MapName + " was not found.");
		} 
		else {
			if (new File(mapWorld, "level.dat").exists()) {
				new File(mapWorld, "uid.dat").delete();
				if (!isCopied(MapName)){
					try {
						setLoadStatus(false);
						Copier.copyFolder(mapWorld, new File(MapName));
					} catch (IOException e) {
						System.out.println("[ERROR] Map couldn't be copied!");
					}
				if (Bukkit.getWorld(MapName) != null){
					Bukkit.getLogger().severe(MapName + " world is still loaded!");
					World world = Bukkit.getWorld(MapName);
					Bukkit.unloadWorld(world, false);
					}
				World w = Bukkit.createWorld(new WorldCreator(MapName));
				w.setAutoSave(false);
				w.setMonsterSpawnLimit(0);
	            setLoadStatus(true);
	            System.out.println("Map Loaded");
				}
	            
	            for (LivingEntity e : Bukkit.getWorld(MapName).getEntitiesByClass(LivingEntity.class))
	                if (!(e instanceof Player))
	                	e.remove();
	            System.out.println("Monsters Removed");

			} 
			else {
				Bukkit.getLogger().warning("[ERROR] Map world could not be found.");
			}	
		}
	}
		
	public boolean isCopied(String MapName) {
		File f = new File(MapName);
		return f.exists() && f.isDirectory();
	}

	public static boolean getLoadStatus(){
		return loaded;
	}
	public static void setLoadStatus(Boolean loaded){
		Main.loaded = loaded;
	}
		*/
}
