package com.snipey92.timt.handler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.snipey92.timt.Main;

public class TeamHandler {
	static Scoreboard sBoard;
	//Scoreboard lobby;
	Objective obj;
	Team Traitors;
	Team Innocents;
	Team Detectives;
	Main plugin;
	
	public void init(){
		sBoard = Bukkit.getScoreboardManager().getNewScoreboard();
		
		obj = sBoard.registerNewObjective("ingame", "teams");
		
		Traitors = sBoard.registerNewTeam("Traitors");
		Detectives = sBoard.registerNewTeam("Detectives");
		
		Traitors.setPrefix(ChatColor.RED + "");
		Detectives.setPrefix(ChatColor.BLUE + "");
		
		Traitors.setAllowFriendlyFire(false);
		Detectives.setAllowFriendlyFire(false);
	}
}
