package com.snipey92.timt.listeners;



import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.snipey92.timt.Main;
import com.snipey92.timt.enums.GameState;
import com.snipey92.timt.functions.TraitorFunctions;
import com.snipey92.timt.stats.Points;
import com.snipey92.timt.updater.Updater;
import com.snipey92.timt.util.Messages;


public class TIMTListener implements Listener {

	private Main plugin;
	private Points points;
	private Updater update;
	public TIMTListener(Main main) {
		this.plugin = main;
		
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Player p = event.getPlayer();
		p.setHealth(20.0);
		p.setFoodLevel(20);
        Location destination = new Location(Bukkit.getWorld(plugin.getConfig().getString(
                "TIMT.spawn.lobby.world")), plugin.getConfig().getInt(
                "TIMT.spawn.lobby.posX"), plugin.getConfig().getInt(
                "TIMT.spawn.lobby.posY"), plugin.getConfig().getInt(
                "TIMT.spawn.lobby.posZ"), plugin.getConfig().getInt(
                "TIMT.spawn.lobby.pitch"), plugin.getConfig().getInt(
                "TIMT.spawn.lobby.yaw"));
			this.plugin.playerlist.add(p.getName());
			p.teleport(destination);
			//update.updateCheck();
		event.setJoinMessage(Messages.prefix + p.getName() + " Has Joined The Game " + ChatColor.GOLD + "(" + ChatColor.GREEN + this.plugin.playerlist.size() + ChatColor.GOLD + "/" + ChatColor.GREEN + "24" + ChatColor.GOLD + ")");
	}
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		Player p = event.getPlayer();
		this.plugin.playerlist.remove(p.getName());
		event.setQuitMessage(Messages.prefix + ChatColor.GOLD + p.getName() + " Has Left The Game " + ChatColor.GOLD + "(" + ChatColor.GREEN + this.plugin.playerlist.size()  + ChatColor.GOLD + "/" + ChatColor.GREEN + "24" + ChatColor.GOLD + ")");

	}
	 @EventHandler
	  public void onPlayerDeath(PlayerDeathEvent event)
	  {
	    Player p = event.getEntity().getPlayer();
	    Location spawnLoc = p.getLocation();

	    if ((this.plugin.playersIngame.containsKey(p)) && (!this.plugin.peace == false)) {
	      event.getDrops().clear();
	      this.plugin.sendArenaMessage(ChatColor.RED + Messages.prefix + " A player has been slain. there are " + ChatColor.GOLD + "(" + ChatColor.GREEN + this.plugin.playerlist.size() + ChatColor.GOLD + "/" + ChatColor.GREEN + "24" + ChatColor.GOLD + ")" + " players remaining.");
	      event.setDeathMessage("");     
	      final Entity entity = Bukkit.getWorld(p.getLocation().getWorld().getName()).spawnEntity(spawnLoc, EntityType.ZOMBIE);
	      String type = "Innocent";
	      if (TraitorFunctions.traitors.contains(p.getName()))
	        type = "Traitor";
	      else if (this.plugin.detectives.contains(p.getName())) {
	        type = "Dectective";
	      }
	      this.plugin.deadInfos.put(entity.getEntityId(), p.getName() + "/" + p.getKiller().getName() + "/" + type);
	      this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
	      {
	        public void run()
	        {
	          Zombie leiche = (Zombie)entity;
	          leiche.setHealth(0);
	        }
	      }
	      , 1200L);

	      if (TraitorFunctions.traitors.contains(p.getName()))
	        TraitorFunctions.traitors.remove(p.getName());
	      else if (this.plugin.detectives.contains(p.getName()))
	        this.plugin.detectives.remove(p.getName());
	      else if (this.plugin.innocent.contains(p.getName())) {
	        this.plugin.innocent.remove(p.getName());
	      }
	      	points.checkTeams();
		    p.kickPlayer(Messages.prefix + "You were killed.");
	    }
	    else if ((this.plugin.playersIngame.containsKey(p)) && (this.plugin.peace == true)) {
			Location gameLoc = new Location(Bukkit.getWorld(this.plugin.getConfig().getString(
					"TIMT.spawn.game.world")), this.plugin.getConfig().getInt(
					"TIMT.spawn.game.posX"), this.plugin.getConfig().getInt(
					"TIMT.spawn.game.posY"), this.plugin.getConfig().getInt(
					"TIMT.spawn.game.posZ"), this.plugin.getConfig().getInt(
					"TIMT.spawn.game.pitch"), this.plugin.getConfig().getInt(
					"TIMT.spawn.game.yaw"));
	      p.teleport(gameLoc);
	      event.setDeathMessage("");
	    }

	  }

	  @EventHandler
	  public void onBlockBreak(BlockBreakEvent event)
	  {
		  Player p = event.getPlayer();
		  if (this.plugin.editmode.contains(p.getName())){
			  
		  }else{
		      event.setCancelled(true);
		  }
	  }

	  @EventHandler
	  public void onBlockPlace(BlockPlaceEvent event)
	  {
		  Player p = event.getPlayer();
		  if (this.plugin.editmode.contains(p.getName())){
			  
		  }else{
		      event.setCancelled(true);
		  }
	  }
		@EventHandler
		public void onLogin(AsyncPlayerPreLoginEvent e){
			Player player = null;
			if(this.plugin.gamestate == GameState.STAFF){
				e.setKickMessage(Messages.prefix + "This server is set to staff only.");
				e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "");
			}else if(this.plugin.gamestate == GameState.STAFF && player.hasPermission("timt.staff")){
				e.allow();
			}
			if(this.plugin.gamestate == GameState.EDIT){
				e.setKickMessage(Messages.prefix + "This server is in edit mode.");
				e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "");
			}
		}
		  @EventHandler
		  public void onPlayerUseCommand(PlayerCommandPreprocessEvent event) {
		    Player p = event.getPlayer();

		    if ((this.plugin.playersIngame.containsKey(p)) && (!event.getMessage().startsWith("/timt")) && (!event.getMessage().startsWith("/tshop")) && (!event.getMessage().startsWith("/traitor"))) {
		      p.sendMessage(ChatColor.RED + Messages.prefix + " You cannot use commands while in a game!");
		      event.setCancelled(true);
		    }
		  }
		  @EventHandler
		  public void onFoodLevelChange(FoodLevelChangeEvent event)
		  {
		    Entity target = event.getEntity();
		    if ((target instanceof Player)) {
		      Player p = (Player)target;
		        event.setCancelled(true);
		    }
		  }
}