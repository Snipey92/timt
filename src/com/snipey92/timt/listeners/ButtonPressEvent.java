package com.snipey92.timt.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import com.snipey92.timt.Main;
import com.snipey92.timt.functions.TraitorFunctions;
import com.snipey92.timt.util.Messages;

public class ButtonPressEvent
  implements Listener
{
  private Main plugin;

  public ButtonPressEvent(Main plugin)
  {
    this.plugin = plugin;
    plugin.getServer().getPluginManager().registerEvents(this, plugin);
  }
  //private List<Integer> traitorTester = new ArrayList<Integer>();
  private boolean traitortester = false;
  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent event)
  {
    Player p = event.getPlayer();
    if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
      Block clicked = event.getClickedBlock();
      if ((clicked.getType() == Material.STONE_BUTTON)){
      	  final ArrayList<Block> list = new ArrayList<Block>();
            if ((this.plugin.playerlist.contains(p)) && (!this.plugin.peace == true) && (!this.traitortester == true)){
            	/*Location middle = sign.getBlock().getLocation();

                 
                for(int x = middle.getBlockX() - 2; x <= middle.getBlockX() + 2; x++)
                {
                    for(int y = middle.getBlockY() - 2; y <= middle.getBlockY() + 2; y++)
                    {
                        for(int z = middle.getBlockZ() - 2; z <= middle.getBlockZ() + 2; z++)
                        {
                            if(x != middle.getBlockX() && y != middle.getBlockY() && z != middle.getBlockZ()){
                            Block block = middle.getWorld().getBlockAt(x, y, z);
                                if(block.getType() == Material.REDSTONE_LAMP_OFF);
                                block.setType(Material.REDSTONE_LAMP_ON);
                                        list.add(block);
                        }
                    }
                }
               
                }
                */
              //int posX = sign.getBlock().getLocation().getBlockX();
              //int posY = sign.getBlock().getLocation().getBlockY();
              //int posZ = sign.getBlock().getLocation().getBlockZ();
              //final Block lampR = p.getLocation().getWorld().getBlockAt(posX - 2, posY + 2, posZ - 1);
              //final Block lampL = p.getLocation().getWorld().getBlockAt(posX - 2, posY + 2, posZ + 2);


              this.plugin.sendArenaMessage(Messages.prefix + ChatColor.GOLD + p.getName() + ChatColor.RED + " has entered the traitor tester. Keep an eye on the lights!");
              this.traitortester = true;
              
              this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable()
              {
                private Main plugin;
                Player p;

				public void run()
                {
                	
                
                  if (TraitorFunctions.traitors.contains(p.getName())) {
                    //lampR.setType(Material.REDSTONE_LAMP_ON);
                    //lampL.setType(Material.REDSTONE_LAMP_ON);
                      /*for (Block b : list) {
                          b.setType(Material.REDSTONE_LAMP_ON);
                          }*/

                    ButtonPressEvent.this.plugin.sendArenaMessage(Messages.prefix + " " + ChatColor.GOLD + p.getName() + ChatColor.RED + " is a traitor. KILL HIM!!!!");
                  } else {
                    ButtonPressEvent.this.plugin.sendArenaMessage(Messages.prefix + ChatColor.GOLD + p.getName() + ChatColor.RED + " is innocent!");
                  }
                  ButtonPressEvent.this.traitortester = false;
                }
              }
              , 140L);
            } else if (this.plugin.peace == true) {
              event.setCancelled(true);
              p.sendMessage(ChatColor.RED + Messages.prefix + " You cannot use the tester during the peace period!");
            
            } else if (this.traitortester == true) {
              event.setCancelled(true);
              p.sendMessage(ChatColor.RED + Messages.prefix + " You can only use the tester if it is not taken by another player in operation!");
            }
            /*this.plugin.getServer().getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable() {
            
            public void run() {
            for (Block b : list) {
            b.setType(Material.REDSTONE_LAMP_OFF);
            }
            }}, 40L);*/
        }}
      }}