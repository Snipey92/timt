package com.snipey92.timt.listeners;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;


import com.snipey92.timt.Main;
import com.snipey92.timt.enums.CountdownState;
import com.snipey92.timt.enums.GameState;
import com.snipey92.timt.util.Messages;

public class ChestListener implements Listener {

	Random generator = new Random();
	public Main plugin;
	int Low = 0;
	int High = 10000;


	public ChestListener(Main main) {
		// TODO Auto-generated constructor stub
		this.plugin = main;
	}


	@EventHandler
	public void onOpenChest(PlayerInteractEvent event) {
		int R = generator.nextInt(High-Low) + Low;
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK
				&& (plugin.gamestate == GameState.INGAME)) {
			BlockState sign = event.getClickedBlock().getState();
			if ((sign instanceof Sign)) {
				Sign schild = (Sign) sign;
				if (((sign instanceof Chest))) {
					Chest chest = (Chest) sign;
					Player p = event.getPlayer();
					if (R < 10000 && 8000 > R) {
						p.closeInventory();
						p.getInventory()
								.addItem(new ItemStack[] { new ItemStack(262, 64) });
						p.getInventory().addItem(new ItemStack[] { new ItemStack(261) });
						p.updateInventory();
						chest.getBlock().setType(Material.AIR);
					}
					if (R < 7999 && 5999 > R) {
						p.closeInventory();
						p.getInventory().addItem(new ItemStack[] { new ItemStack(268) });
						p.updateInventory();
						chest.getBlock().setType(Material.AIR);
					}
					if (R < 5998 && 3998 > R) {
						p.closeInventory();
						p.getInventory().addItem(new ItemStack[] { new ItemStack(272) });
						p.updateInventory();
						chest.getBlock().setType(Material.AIR);
					}
					if (R < 3997 && 1997 > R) {
						p.closeInventory();
						p.getInventory().addItem(new ItemStack[] { new ItemStack(Material.IRON_HELMET) });
						p.updateInventory();
						chest.getBlock().setType(Material.AIR);
					}
					if (R < 1997 && 0 > R) {
						p.closeInventory();
						p.getInventory().addItem(new ItemStack[] { new ItemStack(272) });
						p.updateInventory();
						chest.getBlock().setType(Material.AIR);
					}

				}else {
						event.setCancelled(true);
					}

				if ((sign.getBlock().getType() == Material.ENDER_CHEST)
						&& plugin.state == CountdownState.GAME_STARTED) {
					Player p = event.getPlayer();
					if ((!this.plugin.peace == true)) {
						if (!p.getInventory().contains(new ItemStack(267))) {
							sign.getBlock().setType(Material.AIR);
							p.getInventory().addItem(
									new ItemStack[] { new ItemStack(267) });
							p.updateInventory();
							this.plugin.playersFinalBox.add(p.getName());
						}
					} else if (this.plugin.playersFinalBox.contains(p.getName())) {
						p.sendMessage(Messages.prefix
								+ " You can use only one final box.");
						event.setCancelled(true);
					}
				}else{
					event.setCancelled(true);
				}
			}
		}
	

		
	}
}