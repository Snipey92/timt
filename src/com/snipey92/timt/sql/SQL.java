package com.snipey92.timt.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.snipey92.timt.Main;
import com.snipey92.timt.sql.options.DatabaseOptions;
import com.snipey92.timt.sql.options.MySQLOptions;
import com.snipey92.timt.sql.options.SQLiteOptions;

public class SQL {

public static int q;

private DatabaseOptions dop;
private Connection con;

public SQL(Main plugin, DatabaseOptions dop) {
this.dop = dop;
plugin.getDataFolder().mkdirs();

if (dop instanceof SQLiteOptions) {
try {
((SQLiteOptions) dop).getSQLFile().createNewFile();
} catch (IOException e) {
e.printStackTrace();
}
}
}

public static void resetCount() {
q = 0;
}

public DatabaseOptions getDatabaseOptions() {
return dop;
}

public Connection getConnection() {
return con;
}

public boolean open() throws SQLException {
try {
Class.forName("org.sqlite.JDBC");
} catch (ClassNotFoundException e) {
e.printStackTrace();
con = null;
return false;
}
if (dop instanceof MySQLOptions){
this.con = DriverManager.getConnection("jdbc:mysql://"+((MySQLOptions) dop).getHostname()+":"+
((MySQLOptions) dop).getPort()+"/"+
((MySQLOptions) dop).getDatabase(),
((MySQLOptions) dop).getUsername(),
((MySQLOptions) dop).getPassword());
return true;
}
else if (dop instanceof SQLiteOptions) {
this.con = DriverManager.getConnection("jdbc:sqlite:"+((SQLiteOptions) dop).getSQLFile().getAbsolutePath());
return true;
} else {
return false;
}
}

public void close() throws SQLException {
con.close();
}

public boolean reload() {
try {
close();
return open();
} catch (SQLException e) {
return false;
}
}

public ResultSet query(String query) throws SQLException {
Statement st = null;
ResultSet rs = null;
st = con.createStatement();
q++;
if (query.toLowerCase().contains("delete") || query.toLowerCase().contains("update") || query.toLowerCase().contains("insert")) {
st.executeUpdate(query);
return rs;
} else {
rs = st.executeQuery(query);
return rs;
}
}

public boolean createTable(String table) throws SQLException {
Statement st = con.createStatement();
return st.execute(table);
}
}