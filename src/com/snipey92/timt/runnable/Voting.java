package com.snipey92.timt.runnable;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import com.snipey92.timt.Main;
import com.snipey92.timt.enums.CountdownState;
import com.snipey92.timt.util.Messages;

public class Voting extends BukkitRunnable {
	private Main plugin;
	private int task;

	public Voting(Main main) {
		this.plugin = main;
		task = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), this, 20L, 20L).getTaskId();
	}

	int countdown = 30;

	
	@Override
	public void run() {
		if (Bukkit.getOnlinePlayers().length < 2) {
			plugin.state = CountdownState.COUNTDOWN_CANCELED;
			this.cancelThis();
		}else{

		}

		
		if(!(plugin.state == CountdownState.GAME_STARTED) && plugin.state == CountdownState.COUNTING_DOWN){
			switch (countdown) {
	        case 5:
	        	plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");
		          break;
	        case 4:
	        	plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");
		          break;
	        case 3:
	        	plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");
		          break;
	        case 2:
	        	plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");
		          break;
	        case 1:
	        	plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ countdown + " Second" + ChatColor.GREEN
						+ " until the game starts!");
		          break;
	        case 0:
					this.plugin.sendArenaMessage(ChatColor.GREEN
							+ Messages.prefix + "The game begins"
							+ ChatColor.GOLD + " NOW" + ChatColor.GREEN + "!");
					plugin.state = CountdownState.GAME_STARTED;
			this.plugin.voting();
			plugin.voting = com.snipey92.timt.enums.Voting.INPROGRESS;
					cancelThis();
	        }
	        countdown--;
			
			
			/*if(countdown == 0){
				this.plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "The game begins"
						+ ChatColor.GOLD + " NOW" + ChatColor.GREEN + "!");
				plugin.state = CountdownState.GAME_STARTED;
				this.plugin.startGame();
				cancelThis();
			}
			else if(countdown > 0 && countdown < 6){
				this.plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ this.countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");
			}
			else if (countdown == 10){
				this.plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ this.countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");

			}	
			else if (countdown == 30){
				this.plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ this.countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");

			}else if (countdown == 60){
				this.plugin.sendArenaMessage(ChatColor.GREEN
						+ Messages.prefix + "" + ChatColor.GOLD
						+ this.countdown + " Seconds" + ChatColor.GREEN
						+ " until the game starts!");

			}
			countdown-=1;
		}*/
			
		}
	}

	public void cancelThis() {
		Bukkit.getScheduler().cancelTask(task);
	}
	}


