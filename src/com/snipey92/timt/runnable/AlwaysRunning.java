package com.snipey92.timt.runnable;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.scheduler.BukkitRunnable;

import com.snipey92.timt.Main;
import com.snipey92.timt.enums.CountdownState;
import com.snipey92.timt.enums.Voting;
import com.snipey92.timt.handler.MapHandler;
import com.snipey92.timt.util.Messages;

public class AlwaysRunning extends BukkitRunnable {

	private Main plugin;
	   int sum = 0;
	   int delay = 12;
	   int countdown = 5;

	public AlwaysRunning(Main main) {
		// TODO Auto-generated constructor stub
		this.plugin = main;
	}
	
	@Override
	public void run() {
		if (plugin.voting == Voting.INPROGRESS && plugin.state == CountdownState.VOTING){
			
		 
		     sum = plugin.voteMap1.size() + plugin.voteMap2.size() + plugin.voteMap3.size();  
		    
		    
			this.plugin.x = this.plugin.voteMap1.size();
			this.plugin.y = this.plugin.voteMap2.size();
			this.plugin.z = this.plugin.voteMap3.size();
		      /**/
			if (sum > Bukkit.getOnlinePlayers().length / 2){
				if ( this.plugin.x > this.plugin.y && this.plugin.x > this.plugin.z ){
			    	  this.plugin.selectedMap = this.plugin.map1;
			    	  plugin.state = CountdownState.LOADING;
			    	  MapHandler.load(plugin.selectedMap);
			    	  plugin.voting = Voting.FINISHED;
			    	  plugin.state = CountdownState.COUNTING_DOWN;
			    	  plugin.countdown();
			      }

			       else if ( this.plugin.y > this.plugin.x && this.plugin.y > this.plugin.z ){
				    	  this.plugin.selectedMap = this.plugin.map2;
				    	  plugin.state = CountdownState.LOADING;
				    	  MapHandler.load(this.plugin.selectedMap);
				    	  plugin.voting = Voting.FINISHED;
				    	  plugin.state = CountdownState.COUNTING_DOWN;
							plugin.countdown();

			       }

			       else if ( this.plugin.z > this.plugin.x && this.plugin.z > this.plugin.y ){
				    	  this.plugin.selectedMap = this.plugin.map3;
				    	  plugin.state = CountdownState.LOADING;
				    	  MapHandler.load(this.plugin.selectedMap);
				    	  plugin.voting = Voting.FINISHED;
				    	  plugin.state = CountdownState.COUNTING_DOWN;
							plugin.countdown();
			       }
			}
		}
		if (plugin.state == CountdownState.COUNTDOWN_CANCELED){
			this.plugin.sendArenaMessage(Messages.prefix + "Countdown has been cancelled.");
			plugin.state = CountdownState.RESUME;
		}
		if (delay == 12 && !(plugin.state == CountdownState.GAME_STARTED) && !(plugin.state == CountdownState.COUNTING_DOWN) && !(plugin.state == CountdownState.VOTING) && !(plugin.state == CountdownState.LOADING) && !(plugin.state == CountdownState.COUNTDOWN_CANCELED)) {
			if (Bukkit.getOnlinePlayers().length < 2) {
				this.plugin.sendArenaMessage(Messages.prefix
						+ "There needs to be at least" + ChatColor.GOLD + " 6 "
						+ ChatColor.AQUA + "players in order to start.");
			}
			if (Bukkit.getOnlinePlayers().length > 1 && !(plugin.voting == Voting.INPROGRESS) && !(plugin.voting == Voting.FINISHED) && !(plugin.voting == Voting.POSTVOTE) && !(plugin.voting == Voting.PREVOTE)) {
				plugin.state = CountdownState.VOTING;
				plugin.voting = Voting.INPROGRESS;
				plugin.voting();
			}
			delay = 0;
		}else{
			delay++;
		}
		

			 

		/*if(!(this.plugin.state == CountdownState.LOADING)){
			this.plugin.countdown();
		}*/
		
		
	}

}
